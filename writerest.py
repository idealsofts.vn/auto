import sys
from pathlib import Path

def writeRest(id, num):
	mode = 'w'
	theFile = Path('/tmp/rest-' + id + '.dat')
	if not theFile.exists():
		mode = 'x'
	f = open(theFile, mode)
	f.write(str(num))
	f.close()

windowId = sys.argv[1]
rest = int(sys.argv[2])
writeRest(windowId, rest)
