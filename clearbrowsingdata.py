#!/usr/bin/env python3

import time
from Quartz.CoreGraphics import * 

clearX = 716
clearY = 656

def mouseEvent(type, posx, posy):
	theEvent = CGEventCreateMouseEvent(None, type, (posx,posy), kCGMouseButtonLeft)
	CGEventPost(kCGHIDEventTap, theEvent)

def mouseclickup(posx,posy):
	mouseEvent(kCGEventLeftMouseUp, posx,posy);

def mouseclickdn(posx,posy):
	mouseEvent(kCGEventLeftMouseDown, posx,posy)

def mouseclick(posx,posy):
	mouseclickdn(posx,posy)
	mouseclickup(posx,posy)

# time.sleep(2)
# mouseclick(503, clearY)
# time.sleep(1)
# mouseclick(clearX, clearY)
# time.sleep(2)