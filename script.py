#!/usr/bin/env python3

from operator import mul
from os import error, write
import pyautogui
from setting import *
import traceback
from pynput.keyboard import Controller
from metamaskpass import metamaskpass
from antibot import *

# Mana bar greens' color 
manabg = [
	# Mac screen
	[
		[175, 187], # red
		[110, 121], # green
		[80, 91] # blue
	],
	[
		[185, 204], # red
		[122, 147], # green
		[92, 116] # blue
	],
	[
		[202, 218], # red
		[162, 174], # green
		[129, 144] # blue
	],
	# LG screen
	[
		[158, 196], # red
		[99, 132], # green
		[73, 102] # blue
	]
]

# Error dialog color
errorr = [215, 235]
errorg = [160, 195]
errorb = [145, 175]

# Metamask sign colors
signLockColor = [245, 255]
signUnlockColorR = [0, 60]
signUnlockColorG = [90, 135]
signUnlockColorB = [195, 240]

# Browser reload button
reloadX = 85
reloadY = 82
if hasCacheExt:
	reloadX = 924
	reloadY = 86

# Wallet select button
chooseX = 530
chooseY = 500

# Metamask Sign button
signX = 960
signY = 590
# Metamask Unlock button
unlockX = 780
unlockY = 453
# Metamask password
passwordX = 860
passwordY = 384

# Error dialog
errorX = 660
errorY = 585
errButtonX = 610
errButtonY = 531

errorCloseX = 808
errorCloseY = 247

# First Hero WORK button
workX = 459
workY = 327

workColorY = 317
if multiply == 1:
	workColorY = 316
lastWorkColorY = 627
workColor = [
	# 144, 191, 141
	# 117, 183, 114
	[115, 147],
	[180, 200],
	[110, 148]
]

# Heroes list Close button
closeX = 600
closeY = 246

# Drag position from the 5th hero in the list
dragY = 629

# The length of mouse drag (to the near top of the Heroes list title bar)
dragDeltaY = 380

numOfScreens = numOfHeroes // heroesEachScreen
heroDiff = numOfHeroes % heroesEachScreen
if heroDiff > 0:
	numOfScreens += 1
	manaDistanceOffset = 0
else:
	heroDiff = heroesEachScreen

firstClassY = 342
lastClassY = 653
classXs = [
	238, # Epic
	246, # Rare
	265 # Common
	# Else Super Rare
]
nonClassColor = [
	[230, 250],
	[210, 230],
	[175, 200]
]

heroClasses = ['Epic', 'Rare', 'Common', 'Super Rare']

# The first mana bar position in the heroes list
firstHeroManaStartX = 318
firstHeroManaStartY = 337

# Distance between 2 mana bars
heroManaBarDistanceY = 72
workRestDistanceY = 72

firstHeroWorkIfX = firstHeroManaStartX + manaLen * workIfAbove
firstHeroWorkIfY = firstHeroManaStartY

# The last mana bar position in the heroes list
lastHeroManaStartX = firstHeroManaStartX
lastHeroManaStartY = 647

lastHeroWorkIfX = lastHeroManaStartX + manaLen * workIfAbove
lastHeroWorkIfY = lastHeroManaStartY

# Last Hero WORK button
lastWorkX = workX
lastWorkY = 637

heroRemainX = 308
heroRemainY = 664
heroRemainOY = 649
heroRemainColor = [
	[240, 250],
	[210, 230],
	[175, 200]
]

def isWorking(image, y):
	color = image.getpixel((workX * multiply, y * multiply))
	print('working color', color)
	return workColor[0][0] <= color[0] <= workColor[0][1] \
		and workColor[1][0] <= color[1] <= workColor[1][1] \
		and workColor[2][0] <= color[2] <= workColor[2][1]

def isWorkable(color):
	result = True
	for i in range(0, len(manabg)):
		if manabg[i][0][0] <= color[0] <= manabg[i][0][1] \
			and manabg[i][1][0] <= color[1] <= manabg[i][1][1] \
			and manabg[i][2][0] <= color[2] <= manabg[i][2][1]:
			result = False
			break
	return result and not isStuck(color)

def isLaunchError(color):
	return errorr[0] <= color[0] <= errorr[1] \
		and errorg[0] <= color[1] <= errorg[1] \
		and errorb[0] <= color[2] <= errorb[1]

def isBtnError(color):
	return connectColorR[0] <= color[0] <= connectColorR[1] \
		and connectColorG[0] <= color[1] <= connectColorG[1] \
		and connectColorB[0] <= color[2] <= connectColorB[1]

def getHeroClass(image, posy):
	for i in range(0, len(classXs)):
		heroColor = image.getpixel((classXs[i] * multiply, posy))
		# pyautogui.moveTo(windowLeft + posx / multiply, (windowTop - topOffset) +  posy / multiply)
		# print('hero color', heroColor)
		# time.sleep(1)
		if nonClassColor[0][0] <= heroColor[0] <= nonClassColor[0][1] \
			and nonClassColor[1][0] <= heroColor[1] <= nonClassColor[1][1] \
			and nonClassColor[2][0] <= heroColor[2] <= nonClassColor[2][1]:
			return i
	return len(heroClasses) - 1

def getHeroManaPos(heroClass, index, offset):
	pos = [firstHeroManaStartX + manaLen * classWorkIf[heroClass], (firstHeroWorkIfY + offset + heroManaBarDistanceY * index)]
	return tuple(pos)

def getManaColor(image, pos):
	return image.getpixel((pos[0] * multiply, pos[1] * multiply))

def getLastHeroManaPos(heroClass, index):
	pos = [firstHeroWorkIfX, (lastHeroWorkIfY - heroManaBarDistanceY * (heroesEachScreen - (index + 1)))]
	if heroClass >= 0:
		pos[0] = (firstHeroManaStartX + manaLen * classWorkIf[heroClass])
	return tuple(pos)

def getRevManaColor(image, pos):
	return image.getpixel(pos)
  
def isMetamaskLoaded():
	elapsed = 0
	while elapsed < timeout:
		image = ImageGrab.grab(bbox = bbox)
		metaMaskSignColor = image.getpixel((signX * multiply, signY * multiply))
		metaMaskUnlockColor = image.getpixel((unlockX * multiply, unlockY * multiply))
		if signLockColor[0] <= metaMaskSignColor[0] <= signLockColor[1] \
			and signLockColor[0] <= metaMaskSignColor[1] <= signLockColor[1] \
			and signLockColor[0] <= metaMaskSignColor[2] <= signLockColor[1] \
			and signLockColor[0] <= metaMaskUnlockColor[0] <= signLockColor[1] \
			and signLockColor[0] <= metaMaskUnlockColor[1] <= signLockColor[1] \
			and signLockColor[0] <= metaMaskUnlockColor[2] <= signLockColor[1]:
			time.sleep(2)
			elapsed += 2
		else:
			break
	return elapsed < timeout

def isMetamaskErrorShown():
	return False

def isMetamaskLocked(signColor, unlockColor):
	return signLockColor[0] <= signColor[0] <= signLockColor[1] \
		and signLockColor[0] <= signColor[1] <= signLockColor[1] \
		and signLockColor[0] <= signColor[2] <= signLockColor[1] \
		and signUnlockColorR[0] <= unlockColor[0] <= signUnlockColorR[1] \
		and signUnlockColorG[0] <= unlockColor[1] <= signUnlockColorG[1] \
		and signUnlockColorB[0] <= unlockColor[2] <= signUnlockColorB[1]

def type_string_with_delay(string):
	keyboard = Controller()
	for character in string:  # Loop over each character in the string
		keyboard.type(character)  # Type the character
		delay = uniform(0.01, 0.2)
		time.sleep(delay)  # Sleep for the amount of seconds generated

def login(loginFlag):
	if loginFlag == 0:
		return 0

	if loginFlag == 1:
		# Reload the browser
		moveToAndClick(reloadX, reloadY)
		delay(6)

		elapsed = 0
		# Check page loaded
		while elapsed < timeout:
			image = ImageGrab.grab(bbox=bbox)
			connectColor = image.getpixel((connectX * multiply, connectY * multiply))
			print('connect color', connectColor)
			if connectColorR[0] <= connectColor[0] <= connectColorR[1] \
				and connectColorG[0] <= connectColor[1] <= connectColorG[1] \
				and connectColorB[0] <= connectColor[2] <= connectColorB[1]:
				break
			time.sleep(1)
			elapsed += 1

		if elapsed >= timeout: # timeout
			print('Page load timeout:', elapsed, 'seconds')
			writeError()
			writeRefresh('t')
			sys.exit()
	else:
		moveToAndClick(backX + 300, backY, 50)
		delay(0.5)

	# Click Connect
	moveToAndClick(539, 614, 129, 41)
	delay(4)

	# Load metamask popup
	if not isMetamaskLoaded():
		print('Metamask load timeout')
		writeError()
		writeRefresh('t')
		sys.exit()
	# Check if Metamask locked
	image = ImageGrab.grab(bbox = bbox)
	metaMaskSignColor = image.getpixel((signX * multiply, signY * multiply))
	metaMaskUnlockColor = image.getpixel((unlockX * multiply, unlockY * multiply))
	print('metamask colors', metaMaskSignColor, metaMaskUnlockColor)
	if isMetamaskLocked(metaMaskSignColor, metaMaskUnlockColor):
		print('Metamask locked')
		moveToAndClick(passwordX, passwordY)
		# Put the password
		type_string_with_delay(metamaskpass)
		delay(0.5)
		moveToAndClick(unlockX, unlockY)
		delay(2)
		elapsed = 0
		while elapsed < timeout:
			image = ImageGrab.grab(bbox = bbox)
			metaMaskSignColor = image.getpixel((signX * multiply, signY * multiply))
			if signLockColor[0] > metaMaskSignColor[0] \
				and signLockColor[0] > metaMaskSignColor[1] \
				and signLockColor[0] > metaMaskSignColor[2]:
				break
			delay(1)
			elapsed += 1
		if elapsed >= timeout:
			print('Metamask unlock timeout:', elapsed, 'seconds')
			writeError()
			writeRefresh('t')
			sys.exit()

	moveToAndClick(signX, signY)
	delay(2)

	elapsed = 0
	# Check page loaded
	while elapsed < timeout:
		image = ImageGrab.grab(bbox=bbox)
		color = image.getpixel((heroesX * multiply, heroesY * multiply))
		errorColor = image.getpixel((errorX * multiply, errorY * multiply))
		if not isError(errorColor) and not isStuck(color) and not isMetamaskErrorShown():
			break
		elif isMetamaskErrorShown():
			print('Metamask error')
			moveToAndClick(metamaskErrorButtonX, metamaskErrorButtonY)
			writeError()
			writeRefresh('t')
			sys.exit()
		elif isError(errorColor):
			print('Connect error')
			writeError()
			writeRefresh('t')
			sys.exit()
		time.sleep(1)
		elapsed += 1

	if elapsed >= timeout: # timeout
		print('Metamask sign timeout:', elapsed, 'seconds')
		writeError()
		writeRefresh('t')
		sys.exit()

def loadHero():
	delay(0.5, 0.25)
	elapsed = 0
	while elapsed < timeout:
		image = ImageGrab.grab(bbox=bbox)
		loadingColor = image.getpixel((630 * multiply, 285 * multiply))
		if 90 <= loadingColor[0] <= 120 \
			and 70 <= loadingColor[1] <= 100 \
			and 60 <= loadingColor[2] <= 90:
			time.sleep(1)
			elapsed += 1
		else:
			break
	return elapsed < timeout

def heroesRemain(image):
	color = image.getpixel((heroRemainX * multiply, heroRemainY * multiply))
	color2 = image.getpixel((heroRemainX * multiply, heroRemainOY * multiply))
	print('remain color', color,color2)
	return heroRemainColor[0][0] <= color[0] <= heroRemainColor[0][1] \
		and heroRemainColor[1][0] <= color[1] <= heroRemainColor[1][1] \
		and heroRemainColor[2][0] <= color[2] <= heroRemainColor[2][1] \
		and (color2[0] < heroRemainColor[0][0] or color2[0] > heroRemainColor[0][1]) \
		and (color2[1] < heroRemainColor[1][0] or color2[1] > heroRemainColor[1][1]) \
		and (color2[2] < heroRemainColor[2][0] or color2[2] > heroRemainColor[2][1])

def work():
	writeOK()
	# First, click to the game to get focus
	moveToAndClick(backX + 300, backY, 50)
	delay(0.25)

	# Open heroes list
	if isInGame():
		moveToAndClick(ingameHeroesX, ingameHeroesY, 21, 16)
		delay(0.5)
		moveToAndClick(ingameHeroesX, 690, 23, 34)
	else:
		moveToAndClick(heroesX, heroesY, 19, 24)
	delay(1)

	if not loadHero():
		print('Heroes load timeout')
		writeError()
		writeRefresh('t')
		sys.exit()

	currentWorking = 0
	# Check working status for first 5/10 heroes
	pageNo = 1
	while True:
		image = ImageGrab.grab(bbox=bbox)
		hasRemainHeroes = heroesRemain(image)
		print('hero remain', hasRemainHeroes)
		offset = 0
		rndX = randrange(200, 543)
		rndY = randrange(304, 662)
		moveTo(rndX, rndY)
		for j in range(0, heroesEachScreen):
			if pageNo > 1 and not hasRemainHeroes:
				workingColorY = lastWorkColorY - workRestDistanceY * (heroesEachScreen - (j + 1))
			else:
				workingColorY = workColorY + offset + workRestDistanceY * j
			if not isWorking(image, workingColorY):
				if pageNo > 1 and not hasRemainHeroes:
					heroClass = getHeroClass(image, (lastClassY - heroManaBarDistanceY * (heroesEachScreen - (j + 1))) * multiply)
					manaPos = getLastHeroManaPos(heroClass, j)
				else:
					heroClass = getHeroClass(image, (firstClassY + offset + heroManaBarDistanceY * j) * multiply)
					manaPos = getHeroManaPos(heroClass, j, offset)
				color = getManaColor(image, manaPos)
				# pyautogui.moveTo(windowLeft + manaPos[0], (windowTop - topOffset) + manaPos[1], 0.1)
				print(heroClasses[heroClass] if heroClass >= 0 else 'Common' + ' mana color', color)
				if isWorkable(color):
					if pageNo > 1 and not hasRemainHeroes:
						moveToAndClick(lastWorkX, lastWorkY - workRestDistanceY * (heroesEachScreen - (j + 1)), 16, 9)
						# pyautogui.moveTo(windowLeft + lastWorkX, (windowTop - topOffset) + lastWorkY - workRestDistanceY * (heroesEachScreen - (j + 1)))
					else:
						moveToAndClick(workX, workY + offset + workRestDistanceY * j, 16, 9)
						# pyautogui.moveTo(windowLeft + workX, (windowTop - topOffset) + workY + offset + workRestDistanceY * j)
					delay(0.25, 0.25)
					currentWorking += 1
					# Check error
					image = ImageGrab.grab(bbox=bbox)
					errColor = image.getpixel((errorX * multiply, errorY * multiply))
					errBtnColor = image.getpixel((errButtonX * multiply, errButtonY * multiply))
					if isError(errColor) and not isBtnError(errBtnColor):
						print('Hero work error')
						# Close error and ignore it
						moveToAndClick(errorCloseX, errorCloseY);
						delay(0.5)
						image = ImageGrab.grab(bbox=bbox)
					elif isBtnError(errBtnColor):
						print('Error popup appear')
						writeError()
						writeRefresh('t')
						sys.exit()
				time.sleep(0.25)
			else:
				currentWorking += 1
			if currentWorking >= MAXWORKING:
				break
		if hasRemainHeroes and currentWorking < MAXWORKING:
			dragX = randint(heroesListMinX, heroesListMaxX) + windowLeft
			pyautogui.moveTo(dragX, (windowTop - topOffset) + dragY, 0.5 + getDelayTime())
			pyautogui.mouseDown(button='left')
			pyautogui.moveTo(dragX, (windowTop - topOffset) + dragY - dragDeltaY, uniform(1, 1.1))
			delay(0.75, 0.25)
			pyautogui.mouseUp()
			time.sleep(0.5)
			pageNo += 1
		else:
			break

	print('Current Working', currentWorking)

	# Close Heroes list
	moveToAndClick(closeX, closeY, 15)
	delay(0.5)

	if currentWorking < 1:
		writeRest(0)
		if isInGame():
			moveToAndClick(105, 166, 22, 17)
	else:
		writeRest(86400)
		moveToAndClick(huntX, huntY, 98, 127)
		delay(0.5)
		randomMove()

def needLogin():
	canLogin = 1 # Clear cache and login
	if errFile.exists():
		f = open(errFile, 'r')
		errorLog = f.read()
		f.close()
		print('is error:', errorLog)
		if errorLog != 't': canLogin = 0 # don't need to login
	else:
		canLogin = 0
	needRefresh = isNeedRefresh()
	if not canLogin and needRefresh:
		canLogin = 1
	if not canLogin:
		image = ImageGrab.grab(bbox = bbox)
		erColor = image.getpixel((errorX * multiply, errorY * multiply))
		color = image.getpixel((heroesX * multiply, heroesY * multiply))
		print('Error color:', erColor, 'Stuck color:', color, 'Is stuck:', isStuck(color), 'Is error:', isError(erColor))
		if isStuck(color) or isError(erColor): canLogin = 1
	return canLogin

try:
	loginFlag = needLogin()
	if loginFlag and isInGame():
		loginFlag = 0
	writeRefresh('f')
	print('Need login:', ('Refresh' if loginFlag == 1 else ('Connect' if loginFlag == -1 else 'Working')))
	login(loginFlag)
	work()
except:
	print(traceback.format_exc())
