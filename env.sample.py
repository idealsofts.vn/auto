# Macbook screen resolution 2880x1800
# FullHD screen resolution 1920x1080 set to 1
multiply = 2

# Chrome has Clear cache extension installed or not
hasCacheExt = False

# Timeout
timeout = 60 # seconds

# Total number of active heroes
numOfHeroes = 15

workIfAbove = 0.4 # mana percentage, hero will work when mana is reach 40%
# Mana total width
manaLen = 100
classWorkIf = [
	0.2, # Epic
	0.4, # Rare
	0.5, # Common
	0.3 # Super Rare
]
heroClasses = ['Rare', 'Super Rare', 'Epic']
windowTopOffset = 26

MAXWORKING = 5
