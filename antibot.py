#!/usr/bin/env python3

from PIL import ImageGrab
from layeris.layer_image import LayerImage
import numpy as np
import pyautogui
import os
from setting import *
from PIL import Image

# Anti-Bot

def get_parent_dir(n=1):
	"""returns the n-th parent dicrectory of the current
	working directory"""
	current_path = os.path.dirname(os.path.abspath(__file__))
	for _ in range(n):
		current_path = os.path.dirname(current_path)
	return current_path

# Set up folder names for default values
image_test_folder = os.path.join(get_parent_dir(0), 'captcha')

# Humanize mouse move
def moveTo(x, y, rndX = 0, rndY = 0):
	x += windowLeft
	y += windowTop - topOffset
	if rndY == 0 and not rndX == 0:
		rndY = rndX
	randomXOffset = randrange(-rndX if rndX != 0 else randomMouseOffset[0], rndX if rndX != 0 else randomMouseOffset[1])
	randomYOffset = randrange(-rndY if rndY != 0 else randomMouseOffset[0], rndY if rndY != 0 else randomMouseOffset[1])
	cx, cy = pyautogui.position()
	distance = (abs(x - cx) + abs(y - cy)) / 1500
	if distance < 0.25:
		distance = 0.25
	pyautogui.moveTo(x + randomXOffset, y + randomYOffset, distance + uniform(-0.15, 0.25), pyautogui.easeOutQuad)

def moveToAndClick(x, y, rndX = 0, rndY = 0):
	moveTo(x, y, rndX, rndY)
	delay(0.26, 0.25)
	pyautogui.click()

def randomMove():
	moveCount = randint(-1, 3)
	moved = moveCount
	while moved > 0:
		x = randint(dx * 2, 960)
		y = randint(dy * 2, 600)
		moveTo(x, y)
		moved -= 1

def checkChest():
	# Open chest
	if isInGame():
		moveToAndClick(chestIngameX, chestY)
	else:
		moveToAndClick(chestHomeX, chestY)
	time.sleep(0.1)
	# Move move
	randomRan = False
	rnd = randint(0, 2)
	if rnd == 2:
		x = randint(chestPanelX[0], chestPanelX[1])
		y = randint(chestPanelY[0], chestPanelY[1])
		moveTo(x, y)
		randomRan = True
		rnd = randint(0, 2)
		if rnd == 2:
			x = randint(chestPanelX[0], chestPanelX[1])
			y = randint(chestPanelY[0], chestPanelY[1])
			moveTo(x, y)
		delay(1)
	# Close chest
	if not randomRan:
		delay(1)
	moveToAndClick(chestCloseX, chestCloseY)

def convert_uint_to_float(img_data):
	return img_data / 255

def convert_float_to_uint(img_data):
	return round_to_uint(img_data * 255)

def round_to_uint(img_data):
	return np.round(img_data).astype('uint8')
class NumberCaptcha:
	def __init__(self, captchaBox, answerBox):
		self.captchaBox = captchaBox
		self.answerBox = answerBox

		self.marginLeft = 285
		self.marginRight = 784
		self.topLine = 330
		self.bottomLine = 520
		self.revealerSize = 90
		self.captchaPopupLeft = (289, 340)

		self.captchaBaseLine = 290
		self.captchaLeft = 471
		self.captchaRight = 611

		self.oneWidth = 60
		self.numWidth = 109
		self.number = ''

		self.answerBaseLine = 9

		self.captchaPopupColor = [
			# Red
			[80, 90],
			# Green
			[50, 60],
			# Blue
			[40, 50]
		]
		self.captchaPopupColorLG = [
			# Red
			[160, 180],
			# Green
			[90, 120],
			# Blue
			[65, 95]
		]

		self.sliderHandlerColor = [
			# Red
			[245, 255],
			# Green
			[210, 222],
			# Blue
			[55, 85]
		]

		self.sliderBgColor = [
			# Red
			[118, 135],
			# Green
			[72, 90],
			# Blue
			[54, 72]
		]

		self.sliderHandlerSize = 52
		self.sliderMinX = self.marginLeft
		self.sliderMaxX = 500
		self.sliderBaseY = 580
		self.sliderPos = [0, self.sliderBaseY]
		self.sliderCenter = (540, self.sliderBaseY)
		self.sliderSize = 0
		self.sliderPartCount = 8

		self.failedCount = 0
		self.maxFailed = 2

	def checkCaptcha(self):
		image = ImageGrab.grab(bbox)
		self.__getSliderHandlerPos(image)
		if self.sliderPos[0] > 0 and self.__isCaptchaAvailable(image):
			print('Captcha available')
			image = self.__revealNumber()
			self.__getQuestNumber(image)
			print('Quest number is: {}'.format(self.number))
			self.__processUnlock()
		else:
			print('No captcha')

	def __blackoutImg(self, image):
		for x in range(0, image.width):
			for y in range(0, image.height):
				color = image.getpixel((x, y))
				num = 255 if min(color) > 161 else 0
				newColor = [num, num, num, 255]
				image.putpixel((x, y), tuple(newColor))
		return image
	
	def __revealNumber(self):
		start = time.time()
		marginX = int(self.revealerSize * 0.35)
		marginY = int(self.revealerSize * 0.35)
		x = self.marginLeft + marginX / 2
		captureArea = (
			self.captchaBox[0] / multiply,
			self.captchaBox[1] / multiply,
			(self.captchaBox[2] - self.captchaBox[0]) / multiply,
			(self.captchaBox[3] - self.captchaBox[1]) / multiply
		)

		coords = []
		while x < self.marginRight:
			y = self.topLine + marginY / 2
			xcoords = []
			while y < self.bottomLine:
				rndX = randrange(-3, 3)
				rndY = randrange(-3, 3)
				xcoords.append((windowLeft + x + rndX, (windowTop - topOffset) + y + rndY))
				y += marginY
			coords.append(xcoords)
			x += marginX
		pics = []
		for col in coords:
			rnd = randint(0, 1)
			st = 0 if rnd == 0 else len(col) - 1
			en = len(col) if rnd == 0 else -1
			mn = 1 if rnd == 0 else -1
			for row in range(st, en, mn):
				pyautogui.moveTo(x=col[row][0], y=col[row][1], duration=0, tween=pyautogui.linear, _pause=False)
				os.system('screencapture -x -c -R{},{},{},{}'.format(
					captureArea[0],
					captureArea[1],
					captureArea[2],
					captureArea[3]
				))
				pics.append(ImageGrab.grabclipboard())

		print('Revealed image in {}s'.format(time.time() - start))
		return self.__blendImages(pics, False)

	def __blendImages(self, pics, save = False):
		baseImage = LayerImage(convert_uint_to_float(np.asarray(pics[0])))
		for n in range(1, len(pics)):
			img = convert_uint_to_float(np.asarray(pics[n]))
			baseImage = baseImage.lighten(img)
		if save:
			baseImage.save(os.path.join(image_test_folder, 'x.png'))
		image = self.__blackoutImg(Image.fromarray(convert_float_to_uint(baseImage.image_data)))
		return image

	def __isCaptchaAvailable(self, image):
		sliderBgGrabColor = image.getpixel((self.sliderCenter[0] * multiply, self.sliderCenter[1] * multiply))
		captchaPopupGrabColor = image.getpixel((self.captchaPopupLeft[0] * multiply, self.captchaPopupLeft[1] * multiply))
		# pyautogui.moveTo(windowLeft + self.sliderCenter[0], (windowTop - topOffset) + self.sliderCenter[1], 1)
		# pyautogui.moveTo(windowLeft + self.captchaPopupLeft[0], (windowTop - topOffset) + self.captchaPopupLeft[1], 1)
		print(sliderBgGrabColor, captchaPopupGrabColor)
		return (self.sliderBgColor[0][0] <= sliderBgGrabColor[0] <= self.sliderBgColor[0][1] \
			and self.sliderBgColor[1][0] <= sliderBgGrabColor[1] <= self.sliderBgColor[1][1] \
			and self.sliderBgColor[2][0] <= sliderBgGrabColor[2] <= self.sliderBgColor[2][1]) \
			and (
				(self.captchaPopupColor[0][0] <= captchaPopupGrabColor[0] <= self.captchaPopupColor[0][1] \
				and self.captchaPopupColor[1][0] <= captchaPopupGrabColor[1] <= self.captchaPopupColor[1][1] \
				and self.captchaPopupColor[2][0] <= captchaPopupGrabColor[2] <= self.captchaPopupColor[2][1])
				or ((self.captchaPopupColorLG[0][0] <= captchaPopupGrabColor[0] <= self.captchaPopupColorLG[0][1] \
				and self.captchaPopupColorLG[1][0] <= captchaPopupGrabColor[1] <= self.captchaPopupColorLG[1][1] \
				and self.captchaPopupColorLG[2][0] <= captchaPopupGrabColor[2] <= self.captchaPopupColorLG[2][1]))
			)

	def __getSliderHandlerPos(self, image):
		for x in range(self.sliderMinX, self.sliderMaxX):
			color = image.getpixel((x * multiply, self.sliderBaseY * multiply))
			if self.sliderHandlerColor[0][0] <= color[0] <= self.sliderHandlerColor[0][1] \
				and self.sliderHandlerColor[1][0] <= color[1] <= self.sliderHandlerColor[1][1] \
				and self.sliderHandlerColor[2][0] <= color[2] <= self.sliderHandlerColor[2][1]:
				color = image.getpixel(((x + self.sliderHandlerSize + 1) * multiply, self.sliderBaseY * multiply))
				if self.sliderBgColor[0][0] <= color[0] <= self.sliderBgColor[0][1] \
					and self.sliderBgColor[1][0] <= color[1] <= self.sliderBgColor[1][1] \
					and self.sliderBgColor[2][0] <= color[2] <= self.sliderBgColor[2][1]:
					self.sliderPos[0] = x + self.sliderHandlerSize / 2
					break
		if self.sliderPos[0] > 0:
			self.sliderSize = 2 * (self.sliderCenter[0] - self.sliderPos[0]) # Length of the slide

	def __isNumberBg(self, color):
		return color[0] == color[1] == color[2] and color[0] < 45

	def __getAnswerNumber(self):
		start = time.time()
		captureArea = (
			self.answerBox[0] / multiply,
			self.answerBox[1] / multiply,
			(self.answerBox[2] - self.answerBox[0]) / multiply,
			(self.answerBox[3] - self.answerBox[1]) / multiply
		)
		os.system('screencapture -x -c -R{},{},{},{}'.format(
			captureArea[0],
			captureArea[1],
			captureArea[2],
			captureArea[3]
		))
		image = self.__blackoutImg(ImageGrab.grabclipboard())
		# image.save('./yolo/Data/Source_Images/Test_Images/' + str(time.time()) + '.png')
		number = ''
		x = 0
		while x < image.width / multiply:
			color = image.getpixel((x * multiply, self.answerBaseLine * multiply))
			# print(color)
			if not self.__isNumberBg(color):
				# Found number
				color = image.getpixel(((x + 10) * multiply, self.answerBaseLine * multiply))
				if self.__isNumberBg(color):
					# Number 1 found
					number += '1'
					x += 10
				else:
					color = image.getpixel(((x + 20) * multiply, self.answerBaseLine * multiply))
					if self.__isNumberBg(color):
						# Number 4 found
						number += '4'
						x += 20
					else:
						color = image.getpixel((x * multiply, (self.answerBaseLine + 9) * multiply))
						if self.__isNumberBg(color):
							# Possible numbers: 2, 3, 7
							color = image.getpixel(((x + 1) * multiply, (self.answerBaseLine + 14) * multiply))
							if not self.__isNumberBg(color):
								# Number 2 found
								number += '2'
								x += 31
							else:
								color = image.getpixel(((x + 10) * multiply, (self.answerBaseLine + 15) * multiply))
								if not self.__isNumberBg(color):
									# Number 3 found
									number += '3'
								else:
									# Number 7 found
									number += '7'
								x += 36
						else:
							color = image.getpixel((x * multiply, (self.answerBaseLine + 35) * multiply))
							if self.__isNumberBg(color):
								# Number 5 found
								number += '5'
								x += 36
							else:
								color = image.getpixel((x * multiply, (self.answerBaseLine + 24) * multiply))
								if self.__isNumberBg(color):
									# Number 9 found
									number += '9'
									x += 30
								else:
									color = image.getpixel(((x - 3) * multiply, (self.answerBaseLine + 15) * multiply))
									if self.__isNumberBg(color):
										# Number 8 found
										number += '8'
										x += 31
									else:
										color = image.getpixel(((x + 25) * multiply, (self.answerBaseLine + 15) * multiply))
										if self.__isNumberBg(color):
											# Number 6 found
											number += '6'
										else:
											# Number 0 found
											number += '0'
										x += 30
			if len(number) == 3:
				break
			x += 1
		print('Get answer in {}s'.format(time.time() - start))
		return number

	def test(self, image):
		self.__getQuestNumber(image)

	def __getQuestNumber(self, image):
		start = time.time()
		self.number = ''
		x = 22
		found = False
		while x < 486:
			y = 71
			while y < 190:
				color = image.getpixel((x * multiply, y * multiply))
				# print(color)
				if not self.__isNumberBg(color):
					if not found:
						found = True
						break
					else:
						found = False
					# Found number
					color = image.getpixel(((x + 95) * multiply, (y + 78) * multiply))
					if self.__isNumberBg(color):
						# Possible numbers 1, 4, 7
						color = image.getpixel(((x + 40) * multiply, (y + 28) * multiply))
						color2 = image.getpixel(((x + 44) * multiply, (y + 10) * multiply))
						if not self.__isNumberBg(color):
							self.number += '1'
							x += self.oneWidth
						elif self.__isNumberBg(color2):
							self.number += '4'
							x += self.numWidth
						else:
							self.number += '7'
							x += self.numWidth - 1
						found = False
						break
					else:
						# Remain numbers 0, 1, 2, 3, 5, 6, 8, 9
						color = image.getpixel(((x + 1) * multiply, (y + 94) * multiply))
						if not self.__isNumberBg(color):
							# Possible numbers 1, 2, 3, 5
							color = image.getpixel(((x + 60) * multiply, (y + 94) * multiply))
							if not self.__isNumberBg(color):
								self.number += '2'
								x += self.numWidth
								found = False
								break
							else:
								# Remain numbers 1, 3, 5
								color = image.getpixel(((x + 1) * multiply, (y + 23) * multiply))
								if not self.__isNumberBg(color):
									self.number += '5'
									x += self.numWidth
									found = False
									break
								else:
									# Remain numbers 1, 3
									color = image.getpixel(((x + 67) * multiply, (y + 1) * multiply))
									if not self.__isNumberBg(color):
										self.number += '3'
										x += self.numWidth
									else:
										self.number += '1'
										x += self.oneWidth
									found = False
									break
						else:
							# Remain numbers 0, 6, 8, 9
							color = image.getpixel(((x + 1) * multiply, (y + 37) * multiply))
							if self.__isNumberBg(color):
								self.number += '8'
								x += self.numWidth
								found = False
								break
							else:
								# Remain numbers 0, 6, 9
								color = image.getpixel(((x + 1) * multiply, (y + 50) * multiply))
								if self.__isNumberBg(color):
									self.number += '9'
									x += self.numWidth
									found = False
									break
								else:
									# Remain numbers 0, 6
									color = image.getpixel(((x + 46) * multiply, (y + 35) * multiply))
									if not self.__isNumberBg(color):
										self.number += '6'
									else:
										self.number += '0'
									x += self.numWidth
									found = False
									break
				y += 1
			if len(self.number) == 3:
				break
			x += 1
		print('Get quest number in {}s'.format(time.time() - start))

	def __processUnlock(self):
		start = time.time()
		rndNextY = randrange(-5, 5)
		rndPosX = randrange(-3, 3)
		pyautogui.moveTo(windowLeft + self.sliderPos[0] + rndPosX, (windowTop - topOffset) + self.sliderPos[1] + rndNextY, 0.5 + getDelayTime(), pyautogui.easeOutQuad)
		pyautogui.mouseDown(button='left')
		partSize = self.sliderSize / self.sliderPartCount
		matched = False
		for n in range(0, self.sliderPartCount):
			rndNextY = randrange(-5, 5)
			pyautogui.moveTo(windowLeft + self.sliderPos[0] + (n + 1) * partSize + rndPosX, (windowTop - topOffset) + self.sliderPos[1] + rndNextY, 0.5 + getDelayTime())
			time.sleep(0.1)
			# Capture
			theNumber = self.__getAnswerNumber()
			print('Predicted number:', theNumber)
			if (theNumber == self.number):
				matched = True
				break
		if not matched:
			pyautogui.moveTo(windowLeft + self.sliderPos[0] + rndPosX, (windowTop - topOffset) + self.sliderPos[1] + rndNextY, 0.35 + getDelayTime())
			time.sleep(0.1)
			# Capture
			theNumber = self.__getAnswerNumber()
			print('Predicted number:', theNumber)
			if (theNumber == self.number):
				matched = True
		pyautogui.mouseUp()

		if not matched:
			self.failedCount += 1
			if self.failedCount >= self.maxFailed:
				print('Failed to unlock captcha after', self.maxFailed, 'tried')
				writeError()
				writeRefresh('t')
				sys.exit()
			else:
				delay(0.5)
				print('Retrying...')
				self.checkCaptcha()
		else:
			print('Captcha by passed in {}s'.format(time.time() - start))
			delay(7)

# captchaBox = ((windowLeft + 273) * multiply, ((windowTop - topOffset) + 268) * multiply, (windowLeft + 273 + 533) * multiply, ((windowTop - topOffset) + 268 + 271) * multiply)
# answerBox = ((windowLeft + 470) * multiply, ((windowTop - topOffset) + 282) * multiply, (windowLeft + 616) * multiply, ((windowTop - topOffset) + 331) * multiply)
# myCaptcha = NumberCaptcha(captchaBox, answerBox)
# ima = Image.open(os.path.join(get_parent_dir(0), 'captcha/new/sample7.jpg'))
# ima = ima.resize((ima.width * multiply, ima.height * multiply))
# myCaptcha.test(ima)
# print(myCaptcha.number)