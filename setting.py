#!/usr/bin/env python3

import sys

from pathlib import Path
import time
from PIL import ImageGrab
import traceback
from Quartz.CoreGraphics import * 

from random import uniform, randrange, randint

from env import *

topOffset = 26
try:
	topOffset = windowTopOffset
except NameError:
	topOffset = 26

predefinedClassWorkIf = [
	0.2, # Epic
	0.4, # Rare
	0.5, # Common
	0.3 # Super Rare
]
for n in range(0, len(predefinedClassWorkIf)):
	if n >= len(classWorkIf):
		classWorkIf.append(predefinedClassWorkIf[n])

windowId = sys.argv[1]
windowLeft = int(sys.argv[2])
windowTop = int(sys.argv[3])

dx = (61) * multiply # left of game board
dy = (139) * multiply # top of game board 

# Game board size (resolution)
windowImageResX = 960 * multiply # width
windowImageResY = 600 * multiply # height

manaDistanceOffset = 20

errFile = Path('/tmp/error-' + windowId + '.dat')
newMapFile = Path('/tmp/newmap-' + windowId + '.dat')
needRefreshFile = Path('/tmp/refresh-' + windowId + '.dat')

# Game back button
backX = 119;
backY = 153;

# Open Treasure Hunt
huntX = 551;
huntY = 436;

# Game Heroes button
heroesX = 951;
heroesY = 666;
heroesListMinX = 175
heroesListMaxX = 419

# In game hero list button
ingameHeroesX = 540
ingameHeroesY = 722

# Game connect wallet button
connectX = 650;
connectY = 580;

# Back button color
backColor = [
	[110, 146],
	[170, 195],
	[105, 145]
]

# Connect button color
connectColorR = [230, 260]
connectColorG = [95, 125]
connectColorB = [30, 65]

heroesEachScreen = 5;

randomMouseOffset = [-5, 5] # random mouse position click offset, around 0-5 pixel
randomTimeOsset = [-0.25, 1] # random time delay offset, around 0-1 seconds

# bbox = (0, 0, dx + windowImageResX, dy + windowImageResY)
bbox = (windowLeft * multiply, (windowTop - topOffset) * multiply, windowLeft * multiply + dx + windowImageResX, (windowTop - topOffset) * multiply + dy + windowImageResY)
image = ImageGrab.grab(bbox = bbox)
# Get offset game panel
gameColor = [
	[10, 60],
	[110, 130],
	[10, 40]
]
for nY in range(113, 248):
	foundGame = False
	for nX in range(29, 478):
		gColor = image.getpixel((nX * multiply, nY * multiply))
		if gameColor[0][0] <= gColor[0] <= gameColor[0][1] \
			and gameColor[1][0] <= gColor[1] <= gameColor[1][1] \
			and gameColor[2][0] <= gColor[2] <= gameColor[2][1]:
			newDx = (nX + 20) * multiply
			newDy = (nY + 14) * multiply
			foundGame = True
			print('newDx=',newDx,'newDy=',newDy)
			windowLeft += (newDx - dx) / multiply
			windowTop += (newDy - dy) / multiply
			break
	if foundGame:
		break
# print(nX, nY, foundGame)
# sys.exit()
print('windowLeft', windowLeft, 'windowTop', windowTop)
bbox = (windowLeft * multiply, (windowTop - topOffset) * multiply, windowLeft * multiply + dx + windowImageResX, (windowTop - topOffset) * multiply + dy + windowImageResY)
image = ImageGrab.grab(bbox = bbox)
# sys.exit()

chestIngameX = 913
chestY = 168
chestHomeX = 980
chestCloseX = 809
chestCloseY = 248
chestPanelX = [226, 257]
chestPanelY = [280, 684]

# Error dialog color
errorr = [215, 235]
errorg = [165, 195]
errorb = [145, 175]
errBtnr = [230, 260]
errBtng = [95, 125]
errBtnb = [30, 65]

captchaPopupColor = [
	# Mac
	[
		# Red
		[80, 90],
		# Green
		[50, 60],
		# Blue
		[40, 50]
	],
	# LG
	[
		# Red
		[160, 180],
		# Green
		[90, 110],
		# Blue
		[65, 85]
	]
]

def isStuck(color):
	return (15 <= color[0] <= 29 and 14 <= color[1] <= 20 and 21 <= color[2] <= 30) \
		or (color[0] == 0 and color[1] == 0 and color[2] == 0)

def isError(color):
	return (errorr[0] <= color[0] <= errorr[1] \
		and errorg[0] <= color[1] <= errorg[1] \
		and errorb[0] <= color[2] <= errorb[1]) \
		or (errBtnr[0] <= color[0] <= errBtnr[1] \
		and errBtng[0] <= color[1] <= errBtng[1] \
		and errBtnb[0] <= color[2] <= errBtnb[1])
		# or (captchaPopupColor[0][0][0] <= color[0] <= captchaPopupColor[0][0][1] \
		# and captchaPopupColor[0][1][0] <= color[1] <= captchaPopupColor[0][1][1] \
		# and captchaPopupColor[0][2][0] <= color[2] <= captchaPopupColor[0][2][1]) \
		# or (captchaPopupColor[1][0][0] <= color[0] <= captchaPopupColor[1][0][1] \
		# and captchaPopupColor[1][1][0] <= color[1] <= captchaPopupColor[1][1][1] \
		# and captchaPopupColor[1][2][0] <= color[2] <= captchaPopupColor[1][2][1])

def isConnectButtonAvailable(connectColor):
	return connectColorR[0] <= connectColor[0] <= connectColorR[1] \
		and connectColorG[0] <= connectColor[1] <= connectColorG[1] \
		and connectColorB[0] <= connectColor[2] <= connectColorB[1]

def writeError():
	mode = 'w'
	if not errFile.exists():
		mode = 'x'
	f = open(errFile, mode)
	f.write('t')
	f.close()

def writeOK():
	mode = 'w'
	if not errFile.exists():
		mode = 'x'
	f = open(errFile, mode)
	f.write('f')
	f.close()

def writeNewMap(res):
	mode = 'w'
	if not newMapFile.exists():
		mode = 'x'
	f = open(newMapFile, mode)
	f.write(res)
	f.close()

def isNewMap():
	needRefresh = False
	if newMapFile.exists():
		f = open(newMapFile, 'r')
		needRefresh = True if f.read() == 't' else False
		f.close()
	return needRefresh

def writeRefresh(res):
	mode = 'w'
	if not needRefreshFile.exists():
		mode = 'x'
	f = open(needRefreshFile, mode)
	f.write(res)
	f.close()

def writeRest(num):
	mode = 'w'
	theFile = Path('/tmp/rest-' + windowId + '.dat')
	if not theFile.exists():
		mode = 'x'
	f = open(theFile, mode)
	f.write(str(num))
	f.close()

def isNeedRefresh():
	needRefresh = False
	if needRefreshFile.exists():
		f = open(needRefreshFile, 'r')
		fLog = f.read()
		print('isRefresh:', fLog)
		needRefresh = True if fLog == 't' else False
		f.close()
	return needRefresh

def mouseEvent(type, posx, posy):
	theEvent = CGEventCreateMouseEvent(None, type, (posx,posy), kCGMouseButtonLeft)
	CGEventPost(kCGHIDEventTap, theEvent)

def mousemove(posx,posy):
	mouseEvent(kCGEventMouseMoved, posx,posy)

def mouseclickdn(posx,posy):
	mouseEvent(kCGEventLeftMouseDown, posx,posy)

def mouseclickup(posx,posy):
	mouseEvent(kCGEventLeftMouseUp, posx,posy)

def mousedrag(posx,posy):
	mouseEvent(kCGEventLeftMouseDragged, posx,posy)

def mouseclick(posx, posy, rnd = 0):
	randomOffset = randrange(-rnd if rnd != 0 else randomMouseOffset[0], rnd if rnd != 0 else randomMouseOffset[1])
	mouseclickdn(posx + randomOffset, posy + randomOffset)
	mouseclickup(posx + randomOffset, posy + randomOffset)

def delay(s, maxOffset = 0):
	time.sleep(s + uniform(randomTimeOsset[0], randomTimeOsset[1] if maxOffset == 0 else maxOffset))

def getDelayTime(upper = 0.5):
	return uniform(-0.25, upper)

def isInGame():
	image = ImageGrab.grab(bbox = bbox)
	color = image.getpixel((backX * multiply, backY * multiply))
	print('back color', color)
	return backColor[0][0] <= color[0] <= backColor[0][1] \
		and backColor[1][0] <= color[1] <= backColor[1][1] \
		and backColor[2][0] <= color[2] <= backColor[2][1]
