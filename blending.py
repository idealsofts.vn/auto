from layeris.layer_image import LayerImage
import numpy as np
from PIL import Image
import os
import time

# def convert_uint_to_float(img_data):
#     return img_data / 255

img_path = './yolo/Data/'

import cv2
import pytesseract
import re

image = cv2.imread(os.path.join(img_path, 'x.png'))
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 13, 2)
erode = cv2.erode(thresh, np.array((7, 7)), iterations=1)
text = pytesseract.image_to_string(erode, config="--psm 5")
text = re.sub('[^A-Za-z0-9]+', '\n', text)
print(text)

# start = time.time()
# baseImage = LayerImage.from_file(os.path.join(img_path, '0.png'))
# for n in range(1, 96):
#     img =convert_uint_to_float(np.asarray(Image.open(os.path.join(img_path, str(n) + '.png'))))
#     baseImage = baseImage.lighten(img)

# image1 = LayerImage.from_file('./psd/1.png')
# image2 = LayerImage.from_file('./psd/2.png')
# image3 = LayerImage.from_file('./psd/3.png')
# image4 = LayerImage.from_file('./psd/4.png')
# image5 = LayerImage.from_file('./psd/5.png')
# image6 = Image.open('./psd/5.png')

# image = image1.lighten(image2.image_data)
# image = image.lighten(image3.image_data)
# image = image.lighten(image4.image_data)
# image = image.lighten(image5.image_data)
# image = image.lighten(convert_uint_to_float(np.asarray(image6)))
# baseImage.save('./yolo/Data/Source_Images/Test_Images/x.png')
# print('Image blended in {}s'.format(time.time() - start))
