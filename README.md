# Hướng dẫn

### **Cài đặt**

* Clone zề ***Desktop***
* Python 3
* pip cho python 3

  ```
  python3 get-pip.py
  ```
* Các module

  ```
  pip install pyobjc-core
  pip install pyobjc-framework-Quartz
  pip install image
  pip install pyautogui==0.9.3
  pip install pynput
  ```

### **Cấu hình**

* **auto.scpt**

  * **INTERVAL**: Thời gian check mana (giây)
  * **INTERVAL2**: Thời gian vào lại game tránh Hero ngu như chó (giây)
  * **python_path**: đường dẫn file chạy python3 (có thể check bằng lệnh `which python3`)
* Tạo file **metamaskpass.py** từ **metamaskpass.sample.py**, nhập password ví Metamask đề phòng trường hợp ví bị Locked


### **Chạy thôi!**

- Mở chrome, truy cập vào game, login sẵn ví Metamask
- Kéo cửa sổ Chrome sát góc trên trái màn hình, thu nhỏ sao cho mép phải và dưới sát với đường viền xanh lá của game như hình 😄

  ![](assets/20211124_231927_setup-window.png)
- Trường hợp đang chơi, không cần refresh, chạy script **forceworking.scpt** cho chắc cú
- Trường muốn ép refresh vì lỗi gì gì đó, chạy script **forcelogin.scpt**
- Clone file env, thay đổi tuỳ theo máy và số hero
  ```
  cp env.sample.py env.sample
  ```
  * **multipy**:
    * =***1*** nếu màn hình ngoài (1920x1080)
    * =***2*** nếu màn hình Macbook (2880x1800)
  * **numOfHeroes**: tổng số hero đang có
- Double click **auto.scpt** rồi bấm **Play** và đi ngủ!🚀️
