#!/usr/bin/env python3

from PIL import ImageGrab
from setting import *
from antibot import *
import traceback

# New map button
newMapX = 542;
newMapY = 608;

# Error dialog
errorX = 680
errorY = 585
errButtonX = 610
errButtonY = 531

# Metamask Sign button
signX = 960;
signY = 590;
# Metamask Unlock button
unlockX = 780
unlockY = 453
# Metamask password
passwordX = 860
passwordY = 384
# Metamask error
metamaskErrorButtonX = 673
metamaskErrorButtonY = 377
metamaskErrorBgX = 526
metamaskErrorBgY = 130

newMapR = [230, 260]
newMapG = [95, 125]
newMapB = [30, 65]

signLockColor = [245, 255]
signUnlockColorR = [0, 60]
signUnlockColorG = [90, 135]
signUnlockColorB = [195, 240]

def isMetamaskErrorShown():
	image = ImageGrab.grab(bbox = bbox)
	errorDialogColor = image.getpixel((metamaskErrorBgX * multiply, metamaskErrorBgY * multiply))
	errorButtonColor = image.getpixel((metamaskErrorButtonX * multiply, metamaskErrorButtonY * multiply))
	return signLockColor[0] <= errorDialogColor[0] <= signLockColor[1] \
		and signLockColor[0] <= errorDialogColor[1] <= signLockColor[1] \
		and signLockColor[0] <= errorDialogColor[2] <= signLockColor[1] \
		and signUnlockColorR[0] <= errorButtonColor[0] <= signUnlockColorR[1] \
		and signUnlockColorG[0] <= errorButtonColor[1] <= signUnlockColorG[1] \
		and signUnlockColorB[0] <= errorButtonColor[2] <= signUnlockColorB[1]

def isNewMap(color):
	return newMapR[0] <= color[0] <= newMapR[1] \
		and newMapG[0] <= color[1] <= newMapG[1] \
		and newMapB[0] <= color[2] <= newMapB[1]

try:
	image = ImageGrab.grab(bbox=bbox)

	# If new map button is show -> click it
	color = image.getpixel((newMapX * multiply, newMapY * multiply))
	stuckColor = image.getpixel((heroesX * multiply, heroesY * multiply))
	hasNewMAP = False
	print('new map color', color, 'stuck color', stuckColor)
	if (isNewMap(color)) and not isStuck(stuckColor):
		print('New map available')
		moveToAndClick(backX + 200, backY)
		time.sleep(0.1)
		moveToAndClick(newMapX, 628, 80, 20);
		delay(0.5)

		writeNewMap('t')
		image = ImageGrab.grab(bbox=bbox)
		color = image.getpixel((errorX * multiply, errorY * multiply))
		stuckColor = image.getpixel((heroesX * multiply, heroesY * multiply))
		print('error color', color)
		hasNewMAP = True
	else:
		time.sleep(0.5)
		writeNewMap('f')

	# Check if error appears
	image = ImageGrab.grab(bbox=bbox)
	# If new map button is show -> click it
	color = image.getpixel((errorX * multiply, errorY * multiply))
	stuckColor = image.getpixel((heroesX * multiply, heroesY * multiply))
	if isError(color) or isStuck(stuckColor):
		print('Error popup appear')
		writeError()
		# if hasNewMAP:
		writeRefresh('t')
	else:
		writeOK()
		rnd = randint(0, 1)
		if rnd == 0 and hasNewMAP:
			time.sleep(0.26)
			moveToAndClick(backX + 200, backY, 50) # Get focus
			time.sleep(0.1)
			checkChest()
			rnd = randint(0, 1)
			randomMove()
except:
	print(traceback.format_exc())
