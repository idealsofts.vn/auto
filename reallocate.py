#!/usr/bin/env python3

from setting import *
from antibot import moveToAndClick, checkChest, randomMove

# Error dialog
errorX = 680
errorY = 585
errButtonX = 610
errButtonY = 531

# Error dialog color
errorr = [215, 235]
errorg = [165, 195]
errorb = [150, 175]
errBtnr = [230, 260]
errBtng = [95, 125]
errBtnb = [30, 65]

def isError(color):
	return (errorr[0] <= color[0] <= errorr[1] \
		and errorg[0] <= color[1] <= errorg[1] \
		and errorb[0] <= color[2] <= errorb[1]) \
		or (errBtnr[0] <= color[0] <= errBtnr[1] \
		and errBtng[0] <= color[1] <= errBtng[1] \
		and errBtnb[0] <= color[2] <= errBtnb[1])

try:
	hasClicked = False

	# rnd = randint(0, 1)
	# if rnd == 0:
	# 	hasClicked = True
	# 	moveToAndClick(backX + 200, backY, 50) # Get focus
	# 	delay(0.25)

	if isInGame():
		if not hasClicked:
			hasClicked = True
			moveToAndClick(backX + 200, backY, 50) # Get focus
		moveToAndClick(105, 166, 22, 17)
		delay(5)
		moveToAndClick(huntX, huntY, 98, 127)
		delay(0.5)
		# Check if error appears
		image = ImageGrab.grab(bbox=bbox)
		color = image.getpixel((errorX * multiply, errorY * multiply))
		stuckColor = image.getpixel((heroesX * multiply, heroesY * multiply))
		print('error color', color)
		if isError(color) or isStuck(stuckColor):
			print('Error popup appear')
			writeError()
			# if isNewMap():
			writeRefresh('t')

	if hasClicked:
		randomMove()
except:
	print(traceback.format_exc())
